# osv-scanner pipeline

<!-- BADGIE TIME -->

[![pipeline status](https://img.shields.io/gitlab/pipeline-status/buildgarden/pipelines/osv-scanner?branch=main)](https://gitlab.com/buildgarden/pipelines/osv-scanner/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)
[![cici-tools enabled](https://img.shields.io/badge/%E2%9A%A1_cici--tools-enabled-c0ff33)](https://gitlab.com/buildgarden/tools/cici-tools)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

<!-- END BADGIE TIME -->

> Do not use this software unless you are an active collaborator on the
> associated research project.
>
> This project is an output of an ongoing, active research project. It is
> published without warranty, is subject to change at any time, and has not been
> certified, tested, assessed, or otherwise assured of safety by any person or
> organization. Use at your own risk.

Scan project dependencies for vulnerabilities using [OSV-Scanner](https://google.github.io/osv-scanner/)

- [Project setup](#project-setup)

- [Use cases](#use-cases)

- [Targets](#targets)

- [Variables](#variables)

## Project setup

At a minimum, this pipeline requires that:

- A supported [lockfile](https://google.github.io/osv-scanner/supported-languages-and-lockfiles/#supported-lockfiles) exists in the project.

## Targets

| Name        | Include | Hook | Description                                                                         |
| ----------- | ------- | ---- | ----------------------------------------------------------------------------------- |
| osv-scanner | ✓       |      | Scan for vulnerabilities with [OSV-Scanner](https://google.github.io/osv-scanner/). |

## Use cases

### All includes

Add the following to your `.gitlab-ci.yml` file to enable all targets:

```yaml
include:
  - project: buildgarden/pipelines/osv-scanner
    file:
      - osv-scanner.yml
```

### All hooks

Add the following to your `.pre-commit-config.yaml` file to enable all targets:

```yaml
repos:
  - repo: https://github.com/google/osv-scanner/
    rev: v1.8.1
    hooks:
      - id: osv-scanner
        args: ["-r", "."]
```

## Variables

No variables are required to run the pipeline.
